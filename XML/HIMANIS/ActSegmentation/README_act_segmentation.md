
1 région : Début d'un acte

http://prhlt-kws.prhlt.upv.es/himanis/index.php/ui/show/chancery/79/69



    <Page imageFilename="FRCHANJJ_JJ040_0035R_A.TIF" imageWidth="2921" imageHeight="3803">
        <ReadingOrder>
            <OrderedGroup id="ro_1501243626220" caption="Regions reading order">
                <RegionRefIndexed index="0" regionRef="region_1501243622006_353"/>
            </OrderedGroup>
        </ReadingOrder>
        <TextRegion id="region_1501243622006_353" custom="readingOrder {index:0;}">
            <Coords points="0,0 2921,0 2921,3803 0,3803"/>
        </TextRegion>
    </Page>
    
    
1 région : milieu d'un acte

http://prhlt-kws.prhlt.upv.es/himanis/index.php/ui/show/chancery/79/70

    <Page imageFilename="FRCHANJJ_JJ040_0035V_A.TIF" imageWidth="2921" imageHeight="3803">
        <ReadingOrder>
            <OrderedGroup id="ro_1501243633101" caption="Regions reading order">
                <RegionRefIndexed index="0" regionRef="region_1501243631931_354"/>
            </OrderedGroup>
        </ReadingOrder>
        <TextRegion id="region_1501243631931_354" custom="readingOrder {index:0;}">
            <Coords points="0,0 2921,0 2921,3803 0,3803"/>
        </TextRegion>
    </Page>
    
    

2 régions :  fin d'un acte, début d'un autre

http://prhlt-kws.prhlt.upv.es/himanis/index.php/ui/show/chancery/79/67

    <Page imageFilename="FRCHANJJ_JJ040_0034R_A.TIF" imageWidth="2893" imageHeight="3786">
        <ReadingOrder>
            <OrderedGroup id="ro_1501243576220" caption="Regions reading order">
                <RegionRefIndexed index="0" regionRef="TextRegion_1501243574191_349"/>
                <RegionRefIndexed index="1" regionRef="TextRegion_1501243574191_348"/>
            </OrderedGroup>
        </ReadingOrder>
        <TextRegion id="TextRegion_1501243574191_349" custom="readingOrder {index:0;}">
            <Coords points="2893,1762 0,1762 0,0 2893,0"/>
        </TextRegion>
        <TextRegion id="TextRegion_1501243574191_348" custom="readingOrder {index:1;}">
            <Coords points="2893,3786 0,3786 0,1762 2893,1762"/>
        </TextRegion>
    </Page>


3 régions : fin d'un acte, acte entier, début d'un acte

http://prhlt-kws.prhlt.upv.es/himanis/index.php/ui/show/chancery/79/78

    <Page imageFilename="FRCHANJJ_JJ040_0039V_A.TIF" imageWidth="2877" imageHeight="3822">
        <ReadingOrder>
            <OrderedGroup id="ro_1501243815213" caption="Regions reading order">
                <RegionRefIndexed index="0" regionRef="TextRegion_1501243799249_383"/>
                <RegionRefIndexed index="1" regionRef="TextRegion_1501243813606_387"/>
                <RegionRefIndexed index="2" regionRef="TextRegion_1501243813606_386"/>
            </OrderedGroup>
        </ReadingOrder>
        <TextRegion id="TextRegion_1501243799249_383" custom="readingOrder {index:0;}">
            <Coords points="2877,1133 0,1133 0,0 2877,0"/>
        </TextRegion>
        <TextRegion id="TextRegion_1501243813606_387" custom="readingOrder {index:1;}">
            <Coords points="2877,1703 0,1703 0,1133 2877,1133"/>
        </TextRegion>
        <TextRegion id="TextRegion_1501243813606_386" custom="readingOrder {index:2;}">
            <Coords points="2877,3822 0,3822 0,1703 2877,1703"/>
        </TextRegion>
    </Page>