
Dates : année, mois, jour

<date when="1409-03-24">1409, 24 mars</date>
<date when="1409-08">1409, août</date>
<date when="1477">1477</date>

Dans les champs date, on peut trouver :
15 avril 1351 (n.s.)
février 1355 (n. st.)
S. d.

DS :
* (n. st.) veut dire: "nouveau style", le document dit par exemple "1350", mais comme on ne change pas la date au 1er janvier, on restitue "1351"
* s.d. veut dire "sans date"; "slnd" sans lieu ni date


Dates incertaines :

<date notBefore="1320-12-30" notAfter="1321-01-05">1320, 30 décembre, et 1321, 5 janvier. </date>
<date notBefore="1322-04-16">1322, après le 16 avril. </date>
<origDate notBefore="1351" notAfter="1355">S.d.</origDate></docDate>


Lieux :
