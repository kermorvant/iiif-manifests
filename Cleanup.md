# Nettoyage du dépôt de manifests

1. Suppression de `IIIF-2018/` sur tous les fichiers :
   ``` bash
   find . ! -path './.git/*' -exec sed -i 's/IIIF-2018\///g' {} +
   ```
2. Tri des clés dans les fichiers JSON : un script Python a été utilisé car `json.tool` n'était pas assez complet
   ``` python
   #!/usr/bin/env python3
   """sortjson.py"""
   import sys, json
   with open(sys.argv[1], mode="r+") as f:
       j = json.loads(f.read())
       f.seek(0)
       f.write(json.dumps(j, indent=4, sort_keys=True))
       f.truncate()
   ```
   Pour l'exécuter :
   ``` bash
   find . ! -path './.git/*' -exec python3 sortjson.py {} \;
   ```
3. Passage des manifests via le proxy : utilisation d'un autre script Python
   ``` bash
   python3 sslify.py index.html https://panetios.teklia.com/sslify/
   ```
